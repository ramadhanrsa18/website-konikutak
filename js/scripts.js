/*!
 * jquery.basicPopup
 * 
 * 
 * @author Lucas Dasso
 * @version 1.0.0
 * Copyright 2015. ISC licensed.
 */
var bxsldr;
$(document).ready(function(){
	$('.gallery-section li').click(function(){
		var index = $( this ).index( );
		$.basicpopup({
			content: $('#popup-content').html()
		});
		setTimeout(function(){
			$(document).find('.basicpopup-content').width($(window).width());
			bxsldr = $('.bxslider:first').bxSlider({
				speed: 100
			});
			bxsldr.goToSlide(index);
			$('.animated').addClass('pulse');
		},200);
	});
});